﻿using System;

namespace Si.Wpf.Converters.VisibilityConverters
{
    public class NullToVisibilityConverter : VisibilityConverter
    {
        protected override bool IsVisible(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null;
        }

    }
}
