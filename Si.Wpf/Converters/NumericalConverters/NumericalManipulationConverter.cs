﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;

namespace Si.Wpf.Converters.NumericalConverters
{
    public enum NumericalManipulation
    {
        Add,
        Subtract,
        Multiply,
        Divide,
    }

    public class NumericalManipulationConverter : IMultiValueConverter
    {
        public NumericalManipulation Manipulation { get; set; }

        public NumericalManipulationConverter()
        {
            Manipulation = NumericalManipulation.Add;
        }

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null) return DependencyProperty.UnsetValue;

            var manipulation = Manipulation;
            var doubles = Doubles(values);

            using (var enumerator = doubles.GetEnumerator())
            {
                if (enumerator.MoveNext() == false)
                {
                    // Nothing in collection.
                    return DependencyProperty.UnsetValue;
                }

                var total = enumerator.Current;
                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current;
                    total = ApplyManipulation(total, current, manipulation);
                }

                return total;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { };
        }

        private static IEnumerable<double> Doubles(IEnumerable<object> objects)
        {
            foreach (var value in objects)
            {
                if (value != null)
                {
                    double d;
                    if (double.TryParse(value.ToString(), out d))
                    {
                        yield return d;
                    }
                }
            }
        }

        private static double ApplyManipulation(double first, double second, NumericalManipulation manipulation)
        {
            switch (manipulation)
            {
                case NumericalManipulation.Add:
                    return first + second;
                case NumericalManipulation.Subtract:
                    return first - second;
                case NumericalManipulation.Multiply:
                    return first * second;
                case NumericalManipulation.Divide:
                    return first / second;
                default:
                    return 0;
            }
        }

    }
}
