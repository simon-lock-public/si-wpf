﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Si.Wpf.Converters.NumericalConverters
{
    public enum NumericalRelation
    {
        Equals,
        DoesNotEqual,
        GreaterThan,
        LessThan,
    }

    public abstract class NumericalRelationEvaluator
    {
        public NumericalRelation Relation { get; set; }

        protected NumericalRelationEvaluator()
        {
            Relation = NumericalRelation.Equals;
        }

        public bool CheckRelation(decimal first, decimal second)
        {
            switch (Relation)
            {
                case NumericalRelation.Equals:
                    return first == second;
                case NumericalRelation.DoesNotEqual:
                    return first != second;
                case NumericalRelation.GreaterThan:
                    return first > second;
                case NumericalRelation.LessThan:
                    return first < second;
                default:
                    return false;
            }
        }

    }

    public class NumericalRelationConverter : NumericalRelationEvaluator, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            decimal first;
            decimal second;

            if (value == null ||
                parameter == null ||
                decimal.TryParse(value.ToString(), out first) == false ||
                decimal.TryParse(parameter.ToString(), out second) == false)
            {
                return DependencyProperty.UnsetValue;
            }

            return CheckRelation(first, second);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

    }

    public class NumericalRelationMultiConverter : NumericalRelationEvaluator, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            decimal first;
            decimal second;

            if (values == null ||
                values.Length < 2 ||
                decimal.TryParse(values[0].ToString(), out first) == false ||
                decimal.TryParse(values[1].ToString(), out second) == false)
            {
                return DependencyProperty.UnsetValue;
            }

            return CheckRelation(first, second);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { };
        }

    }
}
