﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Si.Wpf.Service
{
    public class StoryboardBuilder
    {
        public StoryboardBuilder()
        {
            Storyboard = new Storyboard();
        }

        public StoryboardBuilder AddDoubleAnimation
        (
            DependencyObject target,
            string targetProperty,
            double from,
            double to,
            TimeSpan beginTime,
            Duration duration
        )
        {
            return AddDoubleAnimation(target, targetProperty, from, to, beginTime, duration, FillBehavior.HoldEnd);
        }

        public StoryboardBuilder AddDoubleAnimation
        (
            DependencyObject target,
            string targetProperty,
            double from,
            double to,
            TimeSpan beginTime,
            Duration duration,
            FillBehavior fillBehavior
        )
        {
            var animation = new DoubleAnimation
            {
                From = from,
                To = to,
                BeginTime = beginTime,
                Duration = duration,
                FillBehavior = fillBehavior,
            };

            Storyboard.SetTarget(animation, target);
            Storyboard.SetTargetProperty(animation, new PropertyPath(targetProperty));

            Storyboard.Children
                       .Add(animation);

            return this;
        }

        public StoryboardBuilder AddObjectAnimation
        (
            DependencyObject target,
            string targetProperty,
            KeyTime keyTime,
            object value
        )
        {
            var animation = new ObjectAnimationUsingKeyFrames();
            animation.KeyFrames.Add(new DiscreteObjectKeyFrame
            {
                KeyTime = keyTime,
                Value = value,
            });

            Storyboard.SetTarget(animation, target);
            Storyboard.SetTargetProperty(animation, new PropertyPath(targetProperty));

            Storyboard.Children
                       .Add(animation);

            return this;
        }

        public Storyboard Storyboard { get; }

    }
}
