﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Si.Wpf.Collections
{
    public class ObservableStack<T> : INotifyCollectionChanged, IEnumerable<T>, ICollection
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        
        private readonly Stack<T> _stack;

        public ObservableStack() : this (new List<T>()) { }

        public ObservableStack(IEnumerable<T> collection)
        {
            _stack = new Stack<T>(collection);
        }

        public void Push(T item)
        {
            _stack.Push(item);
           
            // When pushing to a stack, every item changes index. Gotta just call reset.
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public T Peek() => _stack.Peek();]

        public T Pop()
        {
            var item = _stack.Pop();

            // When popping from a stack, every item changes index. Gotta just call reset.
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

            return item;
        }

        public bool Any() => _stack.Any();

        public IEnumerable<T> Select() => _stack.Select(x => x);

        public IEnumerable<TResult> Select<TResult>(Func<T, TResult> selector) => _stack.Select(selector);

        public IEnumerator<T> GetEnumerator() => _stack.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public int Count => _stack.Count;

        public void CopyTo(Array array, int index) => ((ICollection)_stack).CopyTo(array, index);
        
        public bool IsSynchronized => ((ICollection)_stack).IsSynchronized;

        public object SyncRoot => ((ICollection)_stack).SyncRoot;

    }
}
