﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Si.Wpf.Collections
{
    public class ObservableQueue<T> : INotifyCollectionChanged, IEnumerable<T>, ICollection
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        
        private readonly Queue<T> _queue;

        public ObservableQueue() : this (new List<T>()) { }

        public ObservableQueue(IEnumerable<T> collection)
        {
            _queue = new Queue<T>(collection);
        }

        public void Enqueue(T item)
        {
            _queue.Enqueue(item);
           
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        public T Dequeue()
        {
            var item = _queue.Dequeue();
            
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

            return item;
        }

        public bool Any() => _queue.Any();

        public IEnumerable<T> Select() => _queue.Select(x => x);

        public IEnumerable<TResult> Select<TResult>(Func<T, TResult> selector) => _queue.Select(selector);

        public IEnumerator<T> GetEnumerator() => _queue.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        
        public int Count => _queue.Count;

        public void CopyTo(Array array, int index) => ((ICollection)_queue).CopyTo(array, index);

        public bool IsSynchronized => ((ICollection)_queue).IsSynchronized;

        public object SyncRoot => ((ICollection)_queue).SyncRoot;

    }
}
